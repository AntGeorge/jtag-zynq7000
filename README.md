# JTAG DRIVER

This is a JTAG library used to read/write JTAG registers and configuration memory/registers.

In order to have better performance and meet the needs of my projects I have changed the hardware controller the library needs. Versions before v3.0 need the [Xilinx AXI2JTAG Controller](https://www.xilinx.com/support/documentation/application_notes/xapp1251-xvc-zynq-petalinux.pdf). While v3.0 uses a custom IP core.

For information about the API check the refman.pdf
