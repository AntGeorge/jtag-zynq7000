/*
 * XC7Z010_1CLG400C.h
 *
 *  Created on: Mar 10, 2021
 *      Author: george
 */

#ifndef SRC_UTILS_JTAG_DEVICES_XC7Z010_1CLG400C_H_
#define SRC_UTILS_JTAG_DEVICES_XC7Z010_1CLG400C_H_

#include "common.h"

#define IR_LEN		6	// JTAG instruction register length of target
#define JC_BEFORE	4	// JTAG chain length before the target
#define JC_AFTER	0	// JTAG chain length after the target

// Configuration memory layout parameters.
#define WPF		101			// Words Per Frame
#define BPW		32			// Bits Per Word
#define BPF		(WPF * BPW)	// Bits Per Frame
#define PLW		0			// Pipeline words

#define DEVICE_IDCODE	0x23727093
#define DEVICE_CIDCODE	0x23727093
#define DEVICE_ARCH		0

/** JTAG Boundary-Scan instructions.
 * 7 Series FPGA Boundary-Scan Instructions as described in Xilinx ug470.
 */
enum BSI_e {
    BSI_EXTEST          = 0x26, ///< Enables boundary-scan EXTEST operation
    BSI_EXTEST_PULSE    = 0x3C, ///< Enables boundary-scan EXTEST_PULSE operation for transceivers
    BSI_EXTEST_TRAIN    = 0x3D, ///< Enables boundary-scan EXTEST_TRAIN operation for transceivers
    BSI_SAMPLE          = 0x01, ///< Enables boundary-scan SAMPLE operation
    BSI_USER1           = 0x02, ///< Access user-defined register 1
    BSI_USER2           = 0x03, ///< Access user-defined register 2
    BSI_USER3           = 0x22, ///< Access user-defined register 3
    BSI_USER4           = 0x23, ///< Access user-defined register 4
    BSI_CFG_OUT         = 0x04, ///< Access the configuration bus for readback
    BSI_CFG_IN          = 0x05, ///< Access the configuration bus for configuration
    BSI_USERCODE        = 0x08, ///< Enables shifting out user code
    BSI_IDCODE          = 0x09, ///< Enables shifting out of ID code
    BSI_HIGHZ_IO        = 0x0A, ///< 3-state IO pins only, while enabling the Bypass register
    BSI_JPROGRAM        = 0x0B, ///< Equivalent to and has the same effect as PROGRAM
    BSI_JSTART          = 0x0C, ///< Clocks the startup sequence when StartClk is TCK
    BSI_JSHUTDOWN       = 0x0D, ///< Clocks the shutdown sequence
    BSI_XADC_DRP        = 0x37, ///< XADC DRP access through JTAG.
    BSI_ISC_ENABLE      = 0x10, ///< Mask the beginning of ISC configuration. Full shutdown is executed
    BSI_ISC_PROGRAM     = 0x11, ///< Enables in-system programming
    BSI_XSC_PROGRAM_KEY = 0x12, ///< Change security status from secure to non-secure mode and vice versa
    BSI_XSC_DNA         = 0x17, ///< Read 57-bit Device DNA value
    BSI_FUSE_DNA        = 0x32, ///< Read 64-bit Device DNA value
    BSI_ISC_NOOP        = 0x14, ///< No operation
    BSI_ISC_DISABLE     = 0x16, ///< Completes ISC configuration. Startup sequence is executed
    BSI_BYPASS          = 0x3F  ///< Enables BYPASS
};

#endif /* SRC_UTILS_JTAG_DEVICES_XC7Z010_1CLG400C_H_ */
