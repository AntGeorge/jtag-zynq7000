/*
 * XCZU9EG.h
 *
 *  Created on: Mar 12, 2022
 *      Author: george
 */

#ifndef SRC_UTILS_JTAG_DEVICES_XCZU9EG_H_
#define SRC_UTILS_JTAG_DEVICES_XCZU9EG_H_

#include "common.h"

#define IR_LEN		12	// JTAG instruction register length of target
#define JC_BEFORE	0	// JTAG chain length before the target
#define JC_AFTER	4	// JTAG chain length after the target

// Configuration memory layout parameters.
#define WPF		93			// Words Per Frame
#define BPW		32			// Bits Per Word
#define BPF		(WPF * BPW)	// Bits Per Frame
#define PLW		25			// Pipeline words

#define DEVICE_IDCODE	0x24738093
#define DEVICE_CIDCODE	0x2484A093
#define DEVICE_ARCH		1

/** JTAG Boundary-Scan instructions.
 * 7 Series FPGA Boundary-Scan Instructions as described in Xilinx ug470.
 */
enum BSI_e {
    BSI_EXTEST          = 0x9A6, ///< Enables boundary-scan EXTEST operation
    BSI_EXTEST_PULSE    = 0x9BC, ///< Enables boundary-scan EXTEST_PULSE operation for transceivers
    BSI_EXTEST_TRAIN    = 0x9BD, ///< Enables boundary-scan EXTEST_TRAIN operation for transceivers
    BSI_SAMPLE          = 0xFC1, ///< Enables boundary-scan SAMPLE operation
    BSI_USER1           = 0x902, ///< Access user-defined register 1
    BSI_USER2           = 0x903, ///< Access user-defined register 2
    BSI_USER3           = 0x922, ///< Access user-defined register 3
    BSI_USER4           = 0x923, ///< Access user-defined register 4
    BSI_CFG_OUT         = 0x904, ///< Access the configuration bus for readback
    BSI_CFG_IN          = 0x905, ///< Access the configuration bus for configuration
    BSI_USERCODE        = 0x908, ///< Enables shifting out user code
    BSI_IDCODE          = 0x249, ///< Enables shifting out of ID code
    BSI_HIGHZ_IO        = 0x28A, ///< 3-state IO pins only, while enabling the Bypass register
    BSI_JPROGRAM        = 0x90B, ///< Equivalent to and has the same effect as PROGRAM
    BSI_JSTART          = 0x90C, ///< Clocks the startup sequence when StartClk is TCK
    BSI_JSHUTDOWN       = 0x90D, ///< Clocks the shutdown sequence
    BSI_ISC_ENABLE      = 0x910, ///< Mask the beginning of ISC configuration. Full shutdown is executed
    BSI_ISC_PROGRAM     = 0x911, ///< Enables in-system programming
    BSI_XSC_DNA         = 0x917, ///< Read 57-bit Device DNA value
    BSI_FUSE_DNA        = 0x932, ///< Read 64-bit Device DNA value
    BSI_ISC_NOOP        = 0x914, ///< No operation
    BSI_ISC_DISABLE     = 0x916, ///< Completes ISC configuration. Startup sequence is executed
    BSI_BYPASS          = 0xFFF, ///< Enables BYPASS
    BSI_JTAG_CTRL       = 0x83F  ///< Enables the PL TAP and ARM DAP controllers onto the JTAG Chain (the PS TAP controller is always present).
};

#endif /* SRC_UTILS_JTAG_DEVICES_XCZU3EG_A484_H_ */
