/** @defgroup platform_conf Platform configuration file
 *
 * jtag_platform.h
 *
 * @date 1 Dec 2019
 * @author AntGeorge
 *
 * @details
 *		This file contains information about the board.
 *		Defines the configuration constraints and some
 *		other import structures which are tied up
 *		with the specific supported architecture Zynq7000.
 *	@{
 */

#ifndef SRC_UTILS_JTAG_DEVICES_COMMON_H_
#define SRC_UTILS_JTAG_DEVICES_COMMON_H_

#include <stdint.h>

#define ONE				0xFFFFFFFF

typedef struct bsi_s {
	uint32_t BSI_EXTEST;
	uint32_t BSI_EXTEST_PULSE;
	uint32_t BSI_EXTEST_TRAIN;
	uint32_t BSI_SAMPLE;
	uint32_t BSI_USER1;
	uint32_t BSI_USER2;
	uint32_t BSI_USER3;
	uint32_t BSI_USER4;
	uint32_t BSI_CFG_OUT;
	uint32_t BSI_CFG_IN;
	uint32_t BSI_USERCODE;
	uint32_t BSI_IDCODE;
	uint32_t BSI_HIGHZ_IO;
	uint32_t BSI_JPROGRAM;
	uint32_t BSI_JSTART;
	uint32_t BSI_JSHUTDOWN;
	uint32_t BSI_ISC_ENABLE;
	uint32_t BSI_ISC_PROGRAM;
	uint32_t BSI_XSC_DNA;
	uint32_t BSI_FUSE_DNA;
	uint32_t BSI_ISC_NOOP;
	uint32_t BSI_ISC_DISABLE;
	uint32_t BSI_BYPASS;
	uint32_t BSI_JTAG_CTRL;
} bsi_t;

typedef enum {
	ZYNQ7000, ULTRASCALE
} arch_t;

typedef struct device_s {
	int ir_len;
	int jc_before;
	int jc_after;

	int wpf;
	int bpw;
	int bpf;
	int pipeline_words;

	arch_t arch;
	bsi_t bsi;
} device_t;

// TODO: Refactor this definition
// This definition is used to specify the number of words in frame_s
// But the WPF migth be different from target's device WPF
#define MAX_WPF 101

#define BYPASS	0xFFFFFFFF

/** Configuration Packets.*/
enum CFG_PACKETS_e {
	TYPE1		= (0x1 << 29),
	TYPE2		= (0x2 << 29),

	OP_NOP		= (0 << 27),
	OP_RD		= (1 << 27),
	OP_WR		= (2 << 27),
	OP_RESERVED	= (3 << 27)
};

// Configuration commands
#define CFG_SYNCWORD	0xAA995566
#define CFG_NOOP		0x20000000
#define CFG_DUMMY		0xFFFFFFFF

/** Configuration registers.*/
enum CFG_REG_e {
	CFG_CRC			= 0x00,	///< CRC Register
	CFG_FAR			= 0x01,	///< Frame Address Register
	CFG_FDRI		= 0x02,	///< Frame Data Register, Input Register (write configuration data)
	CFG_FDRO		= 0x03,	///< Frame Data Register, Output Register (read configuration data)
	CFG_CMD			= 0x04,	///< Command Register
	CFG_CTL0		= 0x05,	///< Control Register 0
	CFG_MASK		= 0x06,	///< Masking Register for CTL0 and CTL1
	CFG_STAT		= 0x07,	///< Status Register
	CFG_LOUT		= 0x08,	///< Legacy Output Register for daisy chain
	CFG_COR0		= 0x09,	///< Configuration Option Register 0
	CFG_MFWR		= 0x0A,	///< Multiple Frame Write Register
	CFG_CBC			= 0x0B,	///< Initial CBC Value Register
	CFG_IDCODE		= 0x0C,	///< Device ID Register
	CFG_AXSS		= 0x0D,	///< User Access Register
	CFG_COR1		= 0x0E,	///< Configuration Option Register 1
	CFG_WBSTAR		= 0x10,	///< Warm Boot Start Address Register
	CFG_TIMER		= 0x11,	///< Watchdog Timer Register
	CFG_BOOTSTS		= 0x16,	///< Boot History Status Register
	CFG_CTL1		= 0x18,	///< Control Register 1
	CFG_BSPI		= 0x1F	///< BPI/SPI Configuration Options Register
};

/** Configuration codes for CMD register.*/
enum CFG_CMD_CODES_e {
	CFG_CMD_NULL		= 0x00,	///< Null command, does nothing
	CFG_CMD_WCFG		= 0x01,	///< Writes Configuration Data: used prior to writing configuration data to the FDRI
	CFG_CMD_MFW			= 0x02,	///< Multiple Frame Write: used to perform a write of a single frame data to multiple frame addresses
	CFG_CMD_DGHIGH		= 0x03,	///< Last Frame: Deasserts the GHIGH_B signal, activating all interconnects. The GHIGH_B signal is asserted with the AGHIGH command
	CFG_CMD_LFRM		= 0x03,	///< same
	CFG_CMD_RCFG		= 0x04,	///< Reads Configuration Data: used prior to reading configuration data from the FDRO
	CFG_CMD_START		= 0x05,	///< Begins the Startup Sequence: The startup sequence begins after a successful CRC check and a DESYNC command are performed
	CFG_CMD_RCAP		= 0x06,	///< Resets the CAPTURE signal after performing readback-capture in single-shot mode
	CFG_CMD_RCRC		= 0x07,	///< Resets CRC: Resets the CRC register
	CFG_CMD_AGHIGH		= 0x08,	///< Asserts the GHIGH_B signal: places all interconnect in a HIGH-Z state to prevent contention when writing new configuration data. This command is only used in shutdown reconfiguration. Interconnect is reactivated with the LFRM command
	CFG_CMD_SWITCH		= 0x09,	///< Switches the CCLK frequency: updates the frequency of the master CCLK to the value specified by the OSCFSEL bits in the COR0 register
	CFG_CMD_GRESTORE	= 0x0A,	///< Pulses the GRESTORE signal: sets/resets (depending on user configuration) IOB and CLB flip-flops
	CFG_CMD_SHUTDOWN	= 0x0B,	///< Begin Shutdown Sequence: Initiates the shutdown sequence, disabling the device when finished. Shutdown activates on the next successful CRC check or RCRC instruction (typically an RCRC instruction)
	CFG_CMD_GCAPTURE	= 0x0C,	///< Pulses GCAPTURE: Loads the capture cells with the current register states
	CFG_CMD_DESYNC		= 0x0D,	///< Resets the DALIGN signal: Used at the end of configuration to desynchronize the device. After desynchronization, all values on the configuration data pins are ignored
	CFG_CMD_RESERVED	= 0x0E,	///< Reserved
	CFG_CMD_IPROG		= 0x0F,	///< Internal PROG for triggering a warm boot
	CFG_CMD_CRCC		= 0x10,	///< When readback CRC is selected, the configuration logic recalculated the first readback CRC value after reconfiguration. Toggling GHIGH has the same effect. This command can be used when GHIGH is not toggled during the reconfiguration case
	CFG_CMD_LTIMER		= 0x11,	///< Reload Watchdog timer
	CFG_CMD_BSPI_READ	= 0x12,	///< BPI/SPI re-initiate bitstream read
	CFG_CMD_FALL_EDGE	= 0x13	///< Switch to negative-edge clocking (configuration data capture on falling edge)
};

/** Defines a configuration word size. */
typedef uint32_t word_t;

#endif /* SRC_UTILS_JTAG_DEVICES_COMMON_H_ */

/** @}*/
