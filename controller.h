/*
 * controller.h
 *
 *  Created on: Sep 24, 2020
 *      Author: george
 */

#ifndef SRC_UTILS_JTAG_CONTROLLER_H_
#define SRC_UTILS_JTAG_CONTROLLER_H_


#define MAX_TCK_DIVIDER		7
#define MAX_TDO_SHIFTS		3

#define MAX_JTAG_SHIFT_IN	((1 << 20) - 16)


// DATA, CONTROL and REPLY FIFO size
// TODO: Driver's view of the  FIFO sizes is less than actual to prevent
// system crash, issue to be fixed/confirmed on validation
#define CONTROL_FIFO_SIZE	(128 - 1)
#define DATA_FIFO_SIZE		(1024 -1)
#define REPLY_FIFO_SIZE		(512 - 1)

// Extract fields from status register
#define BUSY(r)				((r & 0x80000000) >> 31)
#define REPLY_ENTRIES(r)	((r & 0x3FF00000) >> 20)
#define DATA_ENTRIES(r)		((r & 0x7FF00) >> 8)
#define CONTROL_ENTRIES(r)	((r & 0xFF) >> 0)

enum REGISTERS_OFFSET_e {
	// Write Registers
	CONTROL_FIFO_OFFSET	= 0x00,
	DATA_FIFO_OFFSET	= 0x04,
	CALIBRATION_OFFSET	= 0x08,

	// Read Registers
	STATUS_OFFSET		= 0x00,
	REPLY_FIFO_OFFSET	= 0x04
};

#endif /* SRC_UTILS_JTAG_CONTROLLER_H_ */
