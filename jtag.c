/*
 * jtag.c
 *
 *  Created on: 1 Dec 2019
 *      Author: AntGeorge
 */

#include "xtime_l.h"

#include "controller.h"
#include "jtag.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "xil_io.h"

/**
 * Show debug messages.
 * 
 * Level 1: Information messages.
 * Level 2: Above + Debug Values
 */
#define JTAG_DEBUG	0
#define DEBUG_TIME	0
#define TEST_MODE	0

inline static uint32_t min(uint32_t a, uint32_t b)
{
	return a < b ? a : b;
}

inline static void set_control(jtag_t *JtagObj, uint32_t clear_bit, uint32_t ignore_bit, uint64_t shift_in)
{
#if TEST_MODE == 0
	Xil_Out32(JtagObj->periph_addr + CONTROL_FIFO_OFFSET, (clear_bit << 31) | (ignore_bit << 30) | (shift_in & 0xFFFFF));
#endif
}

inline static void set_calibration(jtag_t *JtagObj)
{
#if TEST_MODE == 0
	Xil_Out32(JtagObj->periph_addr + CALIBRATION_OFFSET, (JtagObj->tck_divider << 29) | (JtagObj->extra_tdo_shifts << 24));
#endif
}

inline static void set_data16(jtag_t *JtagObj, uint16_t tdi_value, uint16_t tms_value)
{
#if TEST_MODE == 0
	Xil_Out32(JtagObj->periph_addr + DATA_FIFO_OFFSET, tdi_value << 16 | tms_value);
#endif
}

inline static void set_data32(jtag_t *JtagObj, uint32_t tdi_value, uint32_t tms_value)
{
	set_data16(JtagObj, tdi_value >> 16, tms_value >> 16);
	set_data16(JtagObj, tdi_value, tms_value);
}

inline static uint32_t get_reply(jtag_t *JtagObj)
{
#if TEST_MODE == 0
	return Xil_In32(JtagObj->periph_addr + REPLY_FIFO_OFFSET);
#else
	return 0xDEADBEEF;
#endif
}

inline static uint32_t get_status(jtag_t *JtagObj)
{
#if TEST_MODE == 0
	return Xil_In32(JtagObj->periph_addr + STATUS_OFFSET);
#else
	return 0;
#endif
}

inline static void ignore_data(jtag_t *JtagObj)
{
	JtagObj->ignore_bit = 1;
	JtagObj->clear_bit = 1;
}

inline static void keep_data(jtag_t *JtagObj)
{
	JtagObj->ignore_bit = 0;
	JtagObj->clear_bit = 1;
}

/** Write multiple control entries
 *
 *	@param clear_bit Clears the Control FIFO.
 *	@param ingore_bit Ignore data reception, Reply will not receive TDO data.
 *	@param shift_in Number of bit to configure the control to shift in.
 *  @return The number of bits left to shift, could not push these due to control full.
 */
static int push_control(jtag_t *JtagObj, uint32_t clear_bit, uint32_t ignore_bit, uint64_t shift_in)
{
	int free_control_entries = 0;
	int shifted_bits = 0;
	int remain_shift_in = shift_in;

	free_control_entries = CONTROL_FIFO_SIZE - CONTROL_ENTRIES(get_status(JtagObj));

	while (free_control_entries > 0 && remain_shift_in > 0)
	{
		if (remain_shift_in > MAX_JTAG_SHIFT_IN)
			shifted_bits = MAX_JTAG_SHIFT_IN;
		else
			shifted_bits = remain_shift_in;

		set_control(JtagObj, clear_bit, ignore_bit, shifted_bits);
		remain_shift_in -= shifted_bits;
		free_control_entries--;

		// In order to not mess with the clear and ignore bits
		// Only the first set_control should have the clear_bit provided by the user
		clear_bit = 0;
	}

	return remain_shift_in;
}

/** Make the transaction between AXI2JTAG and DUT.*/
static JRESULT run_command(jtag_t *JtagObj)
{
	if (JtagObj->reg_c == 32) return JOK;

#if JTAG_DEBUG>=2
	xil_printf("TDI: %08x\r\n", JtagObj->tdi_reg);
	xil_printf("TMS: %08x\r\n", JtagObj->tms_reg);
#endif

	uint32_t status;

#if JTAG_DEBUG>=2
	xil_printf("Checking if DATA FIFO & Control FIFO have free space\r\n");
#endif

	// Check if DATA FIFO & Control FIFO have free space.
	do {
		status = get_status(JtagObj);
	} while(CONTROL_ENTRIES(status) >= CONTROL_FIFO_SIZE || DATA_ENTRIES(status) >= DATA_FIFO_SIZE - 1);


#if JTAG_DEBUG>=2
	xil_printf("Pushing data to JTAG controller\r\n");
#endif

	set_calibration(JtagObj);
	set_control(JtagObj, JtagObj->clear_bit, JtagObj->ignore_bit, 32 - JtagObj->reg_c);

	JtagObj->tdi_reg <<= JtagObj->reg_c;
	JtagObj->tms_reg <<= JtagObj->reg_c;
	set_data16(JtagObj, JtagObj->tdi_reg >> 16, JtagObj->tms_reg >> 16);

	if (JtagObj->reg_c < 16)
		set_data16(JtagObj, JtagObj->tdi_reg & 0x0000FFFF, JtagObj->tms_reg & 0x0000FFFF);

	JtagObj->reg_c = 32;

	return JOK;
}

/** Sets tdi_value and tms_value for clocks cycles.
 *
 * NOTE: To set 1 for multiple cycles use the ONE macro.
 *
 * MSB is shifted first.
 *
 * Therefore, if we have tdi_value = 1100 (binary)
 * then the first bit shifting in the jtag will be 1.
 *
 *
 * @param tdi_value
 * @param tms_value
 * @param clocks. Should be less than or equal to 32.
 */
static void set_and_hold(jtag_t *JtagObj, uint32_t tdi_value, uint32_t tms_value, int clocks)
{
	uint32_t tmp_tdi, tmp_tms;

	// TODO remove these two lines & make clock to accept values larger than 32.
	tmp_tdi = (tdi_value << (32 - clocks));
	tmp_tms = (tms_value << (32 - clocks));

	if (clocks <= JtagObj->reg_c) {
		tmp_tdi >>= (32 - clocks);
		tmp_tms >>= (32 - clocks);

		JtagObj->tdi_reg <<= clocks;
		JtagObj->tms_reg <<= clocks;

		JtagObj->tdi_reg |= tmp_tdi;
		JtagObj->tms_reg |= tmp_tms;

		JtagObj->reg_c -= clocks;
	} else {
		JtagObj->tdi_reg <<= JtagObj->reg_c;
		JtagObj->tms_reg <<= JtagObj->reg_c;

		int tmp_reg_c = clocks - JtagObj->reg_c;

		JtagObj->tdi_reg |= (tmp_tdi >> (32 - JtagObj->reg_c));
		JtagObj->tms_reg |= (tmp_tms >> (32 - JtagObj->reg_c));

		JtagObj->reg_c = 0;

		run_command(JtagObj);

		set_and_hold(JtagObj, tdi_value, tms_value, tmp_reg_c);
	}
}

/** Returns the 32bit TDO word from DATA FIFO or -1 if FIFO was empty*/
static uint32_t get_TDO(jtag_t *JtagObj)
{
	uint32_t val;

	while(REPLY_ENTRIES(get_status(JtagObj)) == 0);

#if TEST_MODE == 1
	val = 0xDEADBEEF;
#else
	val = Xil_In32(JtagObj->periph_addr + REPLY_FIFO_OFFSET);
#endif

#if JTAG_DEBUG>=2
	xil_printf("TDO: %08x\r\n", val);
#endif

	return val;
}

/** Moves the state machine from EXIT1-XX to SHIFT-XX.
 *
 * Used when we want to continue shift data or instruction
 * without pass the UPDATE-XX state.
 */
static inline void exit1_to_shift(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0x2, 3);
	if (JtagObj->curr_state == EXIT1_DR)
		JtagObj->curr_state = SHIFT_DR;
	else if (JtagObj->curr_state == EXIT1_IR)
		JtagObj->curr_state = SHIFT_IR;
}

/** Moves the state machine from EXIT1-DR to SHIFT-IR.
 *
 * Used when we have already read or write to jtag register
 * and we want to call another read or write without reset the TAP.
 */
static inline void exit1_DR_to_shift_IR(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0x1C, 5);
	JtagObj->curr_state = SHIFT_IR;
}

/** Moves the state machine from EXIT1-IR to SHIFT-DR.
 *
 * Used after shiftIR(), in order to start shift in
 * or shift out data register.
 */
static inline void exit1_IR_to_shift_DR(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0xC, 4);
	JtagObj->curr_state = SHIFT_DR;
}

/** Moves the state machine from TLR to SHIFT-IR.
 *
 * Used when the first shiftIR() is called.
 */
static inline void TLR_to_shiftIR(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0xC, 5);
	JtagObj->curr_state = SHIFT_IR;
}

/** Moves the state machine from RTI to selectDR
 *
 */
static inline void RTI_to_select_DR(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0x1, 1);
	JtagObj->curr_state = SELECT_DR;
}

/** Moves the state machine from RTI to selectIR
 *
 */
static inline void RTI_to_select_IR(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0x3, 2);
	JtagObj->curr_state = SELECT_IR;
}

/** Moves the state machine from select*R to shift*R
 *
 */
static inline void select_to_shift(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, 0x0, 2);
	if (JtagObj->curr_state == SELECT_DR) {
		JtagObj->curr_state = SHIFT_DR;
	} else if (JtagObj->curr_state == SELECT_IR) {
		JtagObj->curr_state = SHIFT_IR;
	}

}

static void push_dummy(jtag_t *JtagObj)
{
	uint32_t total_pushed = 0, pushed_data = 0, free_data_entries;
	while(total_pushed < (JtagObj->device.wpf + JtagObj->device.pipeline_words)) {
		// Write section
		free_data_entries = DATA_FIFO_SIZE - DATA_ENTRIES(get_status(JtagObj));
		for (pushed_data = 0; pushed_data < (free_data_entries / 2) && total_pushed < (JtagObj->device.wpf + JtagObj->device.pipeline_words); pushed_data++) {
			set_data32(JtagObj, 0, 0);
			total_pushed++;
		}
	}
}

static JRESULT auto_calibration(jtag_t *JtagObj)
{
	uint32_t regc, regj;
	int i;

#if JTAG_DEBUG>=1
	xil_printf("Starting jtag auto calibration...\r\n");
#endif

	for (JtagObj->tck_divider = 0; JtagObj->tck_divider <= MAX_TCK_DIVIDER; JtagObj->tck_divider++) {
		for (JtagObj->extra_tdo_shifts = 0; JtagObj->extra_tdo_shifts <= MAX_TDO_SHIFTS; JtagObj->extra_tdo_shifts++) {
			i = 0;
			do {
				// Read JTAG IDCODE register ONLY, do not use the configuration IDCODE in this stage.
				// If the correct frequency has not been discovered a read to the configuration register
				// will make the DUT device to restart, propably due to JTAG clock > configuration memory frequency
				read_jreg(JtagObj, JtagObj->device.bsi.BSI_IDCODE, 32, &regj);
				regj = reverseBits(regj, 32);
				i += 1;
			} while(i < 1000 && regj == JtagObj->device_idcode);

			if ((regj == JtagObj->device_idcode) && i == 1000) {
#if JTAG_DEBUG>=1
	xil_printf("Auto calibration succeed.\r\n");
#endif
				read_creg(JtagObj, CFG_IDCODE, &regc);
				JtagObj->device_cidcode = regc;
				return JCALOK;
			}
		}
	}

#if JTAG_DEBUG>=1
	xil_printf("Auto calibration failed\r\n");
#endif

	return JCALFAIL;
}

static void load_default_bsi_values(jtag_t *JtagObj)
{
	JtagObj->device.bsi.BSI_EXTEST			= BSI_EXTEST;
	JtagObj->device.bsi.BSI_EXTEST_PULSE 	= BSI_EXTEST_PULSE;
	JtagObj->device.bsi.BSI_EXTEST_TRAIN 	= BSI_EXTEST_TRAIN;
	JtagObj->device.bsi.BSI_SAMPLE 			= BSI_SAMPLE;
	JtagObj->device.bsi.BSI_USER1 			= BSI_USER1;
	JtagObj->device.bsi.BSI_USER2 			= BSI_USER2;
	JtagObj->device.bsi.BSI_USER3 			= BSI_USER3;
	JtagObj->device.bsi.BSI_USER4 			= BSI_USER4;
	JtagObj->device.bsi.BSI_CFG_OUT 		= BSI_CFG_OUT;
	JtagObj->device.bsi.BSI_CFG_IN 			= BSI_CFG_IN;
	JtagObj->device.bsi.BSI_USERCODE 		= BSI_USERCODE;
	JtagObj->device.bsi.BSI_IDCODE 			= BSI_IDCODE;
	JtagObj->device.bsi.BSI_HIGHZ_IO 		= BSI_HIGHZ_IO;
	JtagObj->device.bsi.BSI_JPROGRAM 		= BSI_JPROGRAM;
	JtagObj->device.bsi.BSI_JSTART 			= BSI_JSTART;
	JtagObj->device.bsi.BSI_JSHUTDOWN 		= BSI_JSHUTDOWN;
	JtagObj->device.bsi.BSI_ISC_ENABLE 		= BSI_ISC_ENABLE;
	JtagObj->device.bsi.BSI_ISC_PROGRAM 	= BSI_ISC_PROGRAM;
	JtagObj->device.bsi.BSI_XSC_DNA 		= BSI_XSC_DNA;
	JtagObj->device.bsi.BSI_FUSE_DNA 		= BSI_FUSE_DNA;
	JtagObj->device.bsi.BSI_ISC_NOOP 		= BSI_ISC_NOOP;
	JtagObj->device.bsi.BSI_ISC_DISABLE 	= BSI_ISC_DISABLE;
	JtagObj->device.bsi.BSI_BYPASS 			= BSI_BYPASS;

#ifdef SRC_UTILS_JTAG_DEVICES_XCZU3EG_A484_H_
	JtagObj->device.bsi.BSI_JTAG_CTRL 			= BSI_JTAG_CTRL;
#else
	JtagObj->device.bsi.BSI_JTAG_CTRL 			= 0;
#endif
}

static void load_default_configuration(jtag_t *JtagObj)
{
	JtagObj->device.arch = DEVICE_ARCH;
	JtagObj->device_idcode = DEVICE_IDCODE;
	JtagObj->device_cidcode = DEVICE_CIDCODE;

	JtagObj->device.ir_len = IR_LEN;
	JtagObj->device.jc_before = JC_BEFORE;
	JtagObj->device.jc_after = JC_AFTER;

	JtagObj->device.wpf = WPF;
	JtagObj->device.bpw = BPW;
	JtagObj->device.pipeline_words = PLW;

	JtagObj->mode = INDEPENDENT;

	load_default_bsi_values(JtagObj);
}

JRESULT shiftIR(jtag_t *JtagObj, uint32_t ir)
{
	if (JtagObj->curr_state == EXIT1_IR) {
		exit1_to_shift(JtagObj);
	} else if (JtagObj->curr_state == EXIT1_DR) {
		exit1_DR_to_shift_IR(JtagObj);
	} else if (JtagObj->curr_state == TLR) {
		TLR_to_shiftIR(JtagObj);
	} else if (JtagObj->curr_state == RTI) {
		RTI_to_select_IR(JtagObj);
		select_to_shift(JtagObj);
	} else {
		return JUNEXPSTATE;
	}

	ignore_data(JtagObj);

	// TODO: support daisy chaining multiple FPGAs.
	if (JtagObj->mode == CASCADED) {
		set_and_hold(JtagObj, ONE, 0, JtagObj->device.jc_after);
		if (JtagObj->device.jc_before == 0) {
			set_and_hold(JtagObj, reverseBits(ir, JtagObj->device.ir_len), 0x1, JtagObj->device.ir_len);
		} else {
			set_and_hold(JtagObj, reverseBits(ir, JtagObj->device.ir_len), 0, JtagObj->device.ir_len);
			set_and_hold(JtagObj, ONE, 0x1, JtagObj->device.jc_before);
		}

	} else if (JtagObj->mode == INDEPENDENT) {
		set_and_hold(JtagObj, reverseBits(ir, JtagObj->device.ir_len), 0x1, JtagObj->device.ir_len);
	}
	JtagObj->curr_state = EXIT1_IR;

	return JOK;
}

JRESULT shiftDR(jtag_t *JtagObj, volatile uint32_t *buff, unsigned int num_of_bits, int mode)
{
	if (JtagObj->curr_state == EXIT1_DR) {
		exit1_to_shift(JtagObj);
	} else if (JtagObj->curr_state == EXIT1_IR) {
		exit1_IR_to_shift_DR(JtagObj);
	} else if (JtagObj->curr_state == RTI) {
		RTI_to_select_DR(JtagObj);
		select_to_shift(JtagObj);
	} else {
		return JUNEXPSTATE;
	}

	unsigned int max_words = ceil((double) num_of_bits / 32);
	unsigned int rest_bits = num_of_bits - 32 * (max_words - 1);

	int i;

	// shift buff to register or get from register (mode)
	if (mode == 0) {
		ignore_data(JtagObj);
		run_command(JtagObj);

		if (buff == NULL) {
			for (i = 0; i < max_words - 1; i++) {
				set_and_hold(JtagObj, 0, 0, 32);
				run_command(JtagObj);
			}
		} else {
			keep_data(JtagObj);

			for (i = 0; i < max_words - 1; i++) {
				set_and_hold(JtagObj, 0, 0, 32);
				run_command(JtagObj);
				buff[i] = get_TDO(JtagObj);
			}
		}

		// Get the last bits
		set_and_hold(JtagObj, 0, 0x1, rest_bits);
		run_command(JtagObj);

		if (buff != NULL)
			buff[i] = get_TDO(JtagObj);

		ignore_data(JtagObj);

	} else if (mode == 1) {
		ignore_data(JtagObj);

		for (i = 0; i < max_words - 1; i++) {
			set_and_hold(JtagObj, buff[i], 0, 32);
		}

		// Set the last bits
		set_and_hold(JtagObj, buff[i], 0x1, rest_bits);
	}

	JtagObj->curr_state = EXIT1_DR;
	return JOK;
}

JRESULT shiftDR32(jtag_t *JtagObj, volatile uint32_t buff)
{
	volatile uint32_t tmpbuff[1] = {buff};
	return shiftDR(JtagObj, tmpbuff, 32, 1);
}

void resetTAP(jtag_t *JtagObj)
{
	set_and_hold(JtagObj, 0, ONE, 5);
	JtagObj->curr_state = TLR;
}

void reset_CRC(jtag_t *JtagObj)
{
	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
	shiftDR32(JtagObj, CFG_CMD_RCRC);

	shiftDR32(JtagObj, CFG_DUMMY);
	shiftDR32(JtagObj, CFG_DUMMY);
}

void cfg_sync(jtag_t *JtagObj)
{
	shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);
	shiftDR32(JtagObj, CFG_SYNCWORD);
	shiftDR32(JtagObj, CFG_NOOP);
}

void cfg_desync(jtag_t *JtagObj)
{
	shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);
	//shiftDR32(JtagObj, CFG_DUMMY);
	//shiftDR32(JtagObj, CFG_SYNCWORD);
	//shiftDR32(JtagObj, CFG_NOOP);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
	shiftDR32(JtagObj, CFG_CMD_DESYNC);
	shiftDR32(JtagObj, CFG_NOOP);
	shiftDR32(JtagObj, CFG_NOOP);

	//shiftIR(JtagObj, JtagObj->device.bsi.BSI_BYPASS);
	//shiftIR(JtagObj, JtagObj->device.bsi.BSI_BYPASS);
}

// Reverse len number of bits (from LSB) in num.
uint32_t reverseBits(uint32_t num, int len)
{
	uint32_t NO_OF_BITS = sizeof(num) * 8;
	uint32_t reverse_num = 0, i, temp;

	for (i = 0; i < NO_OF_BITS; i++)
	{
		temp = (num & (1 << i));
		if(temp)
			reverse_num |= (1 << ((NO_OF_BITS - 1) - i));
	}

	return reverse_num >> (32 - len);
}

JRESULT jtag_init(jtag_t *JtagObj, uint32_t base_addr, char *name, uint32_t (*next_frad)(uint32_t), int use_default)
{
	if (use_default) {
		load_default_configuration(JtagObj);
	}

	JtagObj->periph_addr = base_addr;

	JtagObj->device.bpf = JtagObj->device.wpf * JtagObj->device.bpw;

	JtagObj->reg_c = 32;

	JtagObj->tdi_reg = 0;
	JtagObj->tms_reg = 0;

	JtagObj->clear_bit = 1;
	JtagObj->ignore_bit = 1;

	if (next_frad != NULL)
		JtagObj->nextfrad_hook = next_frad;

	int i=0;
	while(name[i] != '\0') {
		JtagObj->name[i] = name[i];
		i++;
	}
	JtagObj->name[i] = '\0';

#if TEST_MODE == 0
	if (auto_calibration(JtagObj) == JCALFAIL) {
		JtagObj->status = JCALFAIL;

		// If autocalibration fail then fallback to the slowest frequency.
		JtagObj->extra_tdo_shifts = 0;
		JtagObj->tck_divider = MAX_TCK_DIVIDER;

		return JCALFAIL;
	}

	uint32_t ret;
	read_jreg(JtagObj, JtagObj->device.bsi.BSI_IDCODE, 32, &ret);
	ret = reverseBits(ret, 32);

	if (ret == JtagObj->device_idcode)
		JtagObj->status = JOK;
	else
		JtagObj->status = JFAIL;
#endif

	return JtagObj->status;
}

/*	JTAG Level Functions	*/

void read_jreg(jtag_t *JtagObj, uint32_t reg, unsigned int n, volatile uint32_t *buff)
{
	resetTAP(JtagObj);
	shiftIR(JtagObj, reg);
	shiftDR(JtagObj, buff, n, 0);
	resetTAP(JtagObj);
	run_command(JtagObj);
}

void write_jreg(jtag_t *JtagObj, uint32_t reg, unsigned int n, volatile uint32_t *buff)
{
	resetTAP(JtagObj);
	shiftIR(JtagObj, reg);
	shiftDR(JtagObj, buff, n, 1);
	resetTAP(JtagObj);
	run_command(JtagObj);
}

void write_jreg32(jtag_t *JtagObj, uint32_t reg, unsigned int n, uint32_t buff)
{
	volatile uint32_t tmpbuff[1] = {buff};
	write_jreg(JtagObj, reg, n, tmpbuff);
}


/*	Configuration Level Functions	*/

static void read_frames_b2b(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[], int capture)
{
	exit1_IR_to_shift_DR(JtagObj);
	run_command(JtagObj);

	int total_pushed = 0,
		pushed_data = 0,

		free_data_entries,
		reply_entries,

		current_frame_index = 0,
		current_word_index = 0;

	set_calibration(JtagObj);

	// Check if control has empty space and push control register
	while (CONTROL_ENTRIES(get_status(JtagObj)) >= CONTROL_FIFO_SIZE);
	set_control(JtagObj, 0, 1, JtagObj->device.bpf + (JtagObj->device.pipeline_words * JtagObj->device.bpw));

	push_dummy(JtagObj);

	int bits_to_shift_in = n * JtagObj->device.bpf;

	uint32_t addr = frad;

	while(current_frame_index < n)
	{
		bits_to_shift_in = push_control(JtagObj, 0, 0, bits_to_shift_in);

		// Write section
		free_data_entries = DATA_FIFO_SIZE - DATA_ENTRIES(get_status(JtagObj));
		reply_entries = REPLY_FIFO_SIZE - REPLY_ENTRIES(get_status(JtagObj));
		// This hot fix patches a bug on hardware.
		// When reply FIFO is full any new write to DATA FIFO will crash the system.
		// After that although the reply FIFO has cleared (from the read section) the axi2jtag stays stuck.
		// To fix this we check both DATA and REPLY FIFOs for free entries and we take the minimum.
		// That way we never have any entry on DATA FIFO when REPLY is full.
		// TODO: Fix the hardware.
		free_data_entries = min(free_data_entries, reply_entries);
		for (pushed_data = 0; pushed_data < (free_data_entries / 2) && total_pushed < (n * JtagObj->device.wpf); pushed_data++)
		{
			set_data32(JtagObj, 0, 0);
			total_pushed++;
		}

		// Read section
		reply_entries = REPLY_ENTRIES(get_status(JtagObj));
		while (reply_entries > 0)
		{
			while (current_word_index < JtagObj->device.wpf && reply_entries > 0)
			{
				buff[current_frame_index].words[current_word_index] = get_reply(JtagObj);
				current_word_index++;
				reply_entries--;
			}
			if (current_word_index == JtagObj->device.wpf)
			{
				buff[current_frame_index].address = addr;
				addr = JtagObj->nextfrad_hook(addr);

				current_frame_index++;
				current_word_index = 0;
			}
		}
	}
}

static void write_frames_b2b(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[])
{

	exit1_to_shift(JtagObj);

	run_command(JtagObj);

	set_calibration(JtagObj);

	int pushed_data = 0, free_data_entries, current_frame_index = 0, current_word_index = 0;

	// Including dummy frame and pipeline words
	uint64_t bits_to_shift_in = JtagObj->device.bpf * (n + 1) + JtagObj->device.pipeline_words * JtagObj->device.bpw;

	while(current_frame_index < n) {

		bits_to_shift_in = push_control(JtagObj, 0, 1, bits_to_shift_in);

		// Write section
		free_data_entries = DATA_FIFO_SIZE - DATA_ENTRIES(get_status(JtagObj));

		for (pushed_data = 0; pushed_data < (free_data_entries / 2)	 &&			// Loop while DATA FIFO has space
			 current_word_index < JtagObj->device.wpf 				 &&			// and loop is on the same frame
			 current_frame_index < n; pushed_data++)
		{											// and have more frames to write
			set_data32(JtagObj, buff[current_frame_index].words[current_word_index], 0);
			current_word_index++;
		}

		if (current_word_index == JtagObj->device.wpf)
		{
			current_frame_index++;
			current_word_index = 0;
		}
	}

	// The size of a dummy frame + pipeline words - 1 word (the last word is appended separately)
	int buffer_size =  JtagObj->device.wpf + JtagObj->device.pipeline_words - 1;

	pushed_data = 0;
	while(pushed_data < buffer_size)
	{
		// Write section
		free_data_entries = DATA_FIFO_SIZE - DATA_ENTRIES(get_status(JtagObj));
		for (; pushed_data < (free_data_entries / 2) && pushed_data < buffer_size; pushed_data++)
		{
			set_data32(JtagObj, 0, 0);
		}
	}

	while(DATA_ENTRIES(get_status(JtagObj)) >= DATA_FIFO_SIZE - 1);
	set_data32(JtagObj, 0, 0x00000001);
	JtagObj->curr_state = EXIT1_DR;
}

void read_creg(jtag_t *JtagObj, uint32_t reg, volatile word_t *buff)
{
	resetTAP(JtagObj);

	cfg_sync(JtagObj);

	shiftDR32(JtagObj, TYPE1 | OP_RD | (reg << 13) | 1);
	shiftDR32(JtagObj, CFG_NOOP);
	shiftDR32(JtagObj, CFG_NOOP);

	shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);
	shiftDR(JtagObj, buff, 32, 0);

	cfg_desync(JtagObj);

	resetTAP(JtagObj);
	run_command(JtagObj);
}

void write_creg(jtag_t *JtagObj, uint32_t reg, word_t buff)
{
	resetTAP(JtagObj);

	cfg_sync(JtagObj);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (reg << 13) | 1);
	shiftDR32(JtagObj, buff);

	cfg_desync(JtagObj);

	resetTAP(JtagObj);
	run_command(JtagObj);
}

void read_frames(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[], int capture)
{
#if DEBUG_TIME==1
	XTime tStart, tEnd;
	XTime_GetTime(&tStart);
#endif

	ignore_data(JtagObj);
	resetTAP(JtagObj);

	cfg_sync(JtagObj);

	if (capture) {
		if (JtagObj->device.arch == ZYNQ7000) {
			shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
			shiftDR32(JtagObj, CFG_CMD_GCAPTURE);
		} else if (JtagObj->device.arch == ULTRASCALE) {
			shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_MASK << 13) | 1);
			shiftDR32(JtagObj, (0x1 << 23));

			shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CTL1 << 13) | 1);
			shiftDR32(JtagObj, (0x1 << 23));
		}
	}

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_FAR << 13) | 1);
	shiftDR32(JtagObj, frad);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
	shiftDR32(JtagObj, CFG_CMD_RCFG);

	shiftDR32(JtagObj, TYPE1 | OP_RD | (CFG_FDRO << 13) | 0);
	shiftDR32(JtagObj, TYPE2 | OP_RD | (((n+1) * JtagObj->device.wpf) + JtagObj->device.pipeline_words));
	shiftDR32(JtagObj, CFG_NOOP);
	shiftDR32(JtagObj, CFG_NOOP);

	shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);

	read_frames_b2b(JtagObj, frad, n, buff, capture);

	if (capture) {
		if (JtagObj->device.arch == ULTRASCALE) {
			shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_MASK << 13) | 1);
			shiftDR32(JtagObj, (0x1 << 23));

			shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CTL1 << 13) | 1);
			shiftDR32(JtagObj, 0);
		}
	}

	resetTAP(JtagObj);
	run_command(JtagObj);

#if DEBUG_TIME == 1
	XTime_GetTime(&tEnd);
	int t = (tEnd - tStart) / (COUNTS_PER_SECOND / (double) 1000000);
	xil_printf("Time to execute read %d frames starting from %08x: %u microseconds\r\n", n, frad, t);
#endif
}

void write_frames(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[], int ff)
{
#if DEBUG_TIME==1
	XTime tStart, tEnd;
	XTime_GetTime(&tStart);
#endif

	ignore_data(JtagObj);
	resetTAP(JtagObj);

	cfg_sync(JtagObj);

	reset_CRC(JtagObj);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_IDCODE << 13) | 1);
	shiftDR32(JtagObj, JtagObj->device_cidcode);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_FAR << 13) | 1);
	shiftDR32(JtagObj, frad);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
	shiftDR32(JtagObj, CFG_CMD_WCFG);

	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_FDRI << 13) | 0);
	shiftDR32(JtagObj, TYPE2 | OP_WR | (((n+1) * JtagObj->device.wpf) + JtagObj->device.pipeline_words));

	write_frames_b2b(JtagObj, frad, n, buff);

	if (ff == 1) {

//		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
//		shiftDR32(JtagObj, CFG_CMD_SHUTDOWN);

		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
		shiftDR32(JtagObj, CFG_CMD_RCRC);

		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
		shiftDR32(JtagObj, CFG_CMD_GRESTORE);

		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
		shiftDR32(JtagObj, CFG_CMD_START);
	}


	cfg_desync(JtagObj);
	resetTAP(JtagObj);
	run_command(JtagObj);

#if DEBUG_TIME == 1
	// For timing analysis, wait until controller has finished.
	while(BUSY(get_status(JtagObj)) == 1);

	XTime_GetTime(&tEnd);
	int t = (tEnd - tStart) / (COUNTS_PER_SECOND / (double) 1000000);
	xil_printf("Time to execute write %d frames starting from %08x: %u microseconds\r\n", n, frad, t);
#endif
}


#undef JTAG_DEBUG
#undef DEBUG_TIME
