/** @defgroup jtag JTAG Engine
 *
 * jtag.h
 *
 * @date 1 Dec 2019
 * @author AntGeorge
 *
 * @details
 *		This module contains the basic communication functions
 *		to read and write in configuration memory & JTAG registers
 *		through JTAG TAP.
 *
 *		It uses a custom core to pass the JTAG commands sequence
 *		outside the FPGA (external controller) using 4 GPIO pins
 *		which must be connected to JTAG header of the other device (DUT).
 *
 *	Tested in devices:
 *		- Zynq7010
 *
 *	@version 1.0
 *
 *	@{
 */

#ifndef SRC_JTAG_ENGINE_H_
#define SRC_JTAG_ENGINE_H_

#include <stdint.h>

#include "devices/XCZU9EG.h"
#include "ff.h"

/** Return values.*/
typedef enum {
	JOK = 0,		///< OK.
	JFAIL,			///< Fail.
	JUNEXPSTATE,	///< Unexpected state.
	JCALFAIL,		///< Auto calibration failed.
	JCALOK			///< Auto calibration succeed.
} JRESULT;

/** JTAG TAP State Machine States.*/
typedef enum {
	TLR, RTI,
	SELECT_DR, CAPTURE_DR, SHIFT_DR, EXIT1_DR, PAUSE_DR, EXIT2_DR, UPDATE_DR,
	SELECT_IR, CAPTURE_IR, SHIFT_IR, EXIT1_IR, PAUSE_IR, EXIT2_IR, UPDATE_IR
} JSTATE;

typedef enum {
	INDEPENDENT, CASCADED
} JTAGMODE;

/** JTAG object structure.*/
typedef struct jtag_s {
	uint32_t device_idcode;					///< Device's JTAG register IDCODE.
	uint32_t device_cidcode;				///< Device's Configuration register IDCODE.
	uint32_t periph_addr;					///< AXI2JTAG address.
	JSTATE curr_state;						///< Current JTAG TAP state.
	JTAGMODE mode;							///< JTAG Mode.
	uint32_t (*nextfrad_hook)(uint32_t);	///< Get next frame hook function.
	device_t device;						///< Device hardware description.

	uint32_t tck_divider;
	uint32_t extra_tdo_shifts;
	JRESULT status;
	uint8_t clear_bit;
	uint8_t ignore_bit;

	char name[20];							///< Human readable name.

	uint32_t tdi_reg;						///< Internal TDI register
	uint32_t tms_reg;						///< Internal TMS register
	uint32_t reg_c;							///< Number of available bit in TDI/TDO registers.
} jtag_t;

/** Defines a frame in configuration. */
typedef struct frame_s frame_t;
struct frame_s {
	word_t address;			///< Frame Address
	word_t words[MAX_WPF];	///< Frame words data
};

/** Moves the state machine to TLR state.*/
void resetTAP(jtag_t *JtagObj);

/** Sync configuration module.*/
void cfg_sync(jtag_t *JtagObj);

/** Desync configuration module.*/
void cfg_desync(jtag_t *JtagObj);

/** Set Instruction Register.
 *
 * Expects to see the TAP state machine in
 * EXIT1-IR or EXIT1-DR or TLR or RTI otherwise it returns JUNEXPSTATE.
 *
 * @param ir Instruction Register value.
 * @return JRESULT.
 */
JRESULT shiftIR(jtag_t *JtagObj, uint32_t ir);

/** Get/Set data from/to current DR.
 *
 * Based on the mode parameter shifts num_of_bits from buff to data register
 * or gets num_of_bits from data register and save to buff.
 *
 * mode = 0: Shift out (Read)
 * mode = 1: Shift in (Write)
 *
 * Before any call to shiftDR, the data register must be selected using shiftIR().
 * If the JTAG State machine is neither in EXIT1-DR nor EXIT1-IR then
 * returns JUNEXPSTATE.
 *
 * @param buff Pointer to buffer to get or store the data.
 * @param num_of_bits Number of bits to read or write.
 * @param mode Read or write functionality.
 * @return JRESULT.
 */
JRESULT shiftDR(jtag_t *JtagObj, volatile uint32_t *buff, unsigned int num_of_bits, int mode);

/** Same as shiftDR(), but only for 32bit words write.*/
JRESULT shiftDR32(jtag_t *JtagObj, volatile uint32_t buff);

/** Initialize JtagObj with the specified configuration.
 *
 * This function sets the mode of jtag to INDEPENDENT.
 *
 * @param base_addr Base address of the physical controller.
 * @param name Human readable name.
 * @param next_frad This argument is crucial if read_frames/write_frames for multiple frames is used.
 * @param use_default Set this parameter to initialize the jtag with the default values.
 * @return JFAIL if it couldn't read IDCODE from the target device.
 */
JRESULT jtag_init(jtag_t *JtagObj, uint32_t base_addr, char *name, uint32_t (*next_frad)(uint32_t), int use_default);

/** Read N bits from JTAG register.
 *
 * @param reg Jtag register code.
 * @param n Number of bits to read.
 * @param buff Buffer to store the data.
 */
void read_jreg(jtag_t *JtagObj, uint32_t reg, unsigned int n, volatile uint32_t *buff);

/** Write N bits from buff to JTAG register.
 *
 * @param reg Jtag register code.
 * @param n Number of bits to write.
 * @param buff Buffer which contains the value.
 */
void write_jreg(jtag_t *JtagObj, uint32_t reg, unsigned int n, volatile uint32_t *buff);

/** Same as write_jreg().
 * Used in case buff is macro.
 */
void write_jreg32(jtag_t *JtagObj, uint32_t reg, unsigned int n, uint32_t buff);

/** Read configuration register.
 *
 * @param reg Configuration register address.
 * @param buff Buffer to save the value.
 */
void read_creg(jtag_t *JtagObj, uint32_t reg, volatile word_t *buff);

/** Write configuration register.
 *
 * @param reg Configuration register address.
 * @param buff Data to be written.
 */
void write_creg(jtag_t *JtagObj, uint32_t reg, word_t buff);

/** Read configuration frame(s).
 *
 * @param frad Frame Address.
 * @param n Number of frames to read.
 * @param buff The buffer to save the frames.
 * @param capture Readback or readback capture.
 */
void read_frames(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[], int capture);

/** Write configuration frame(s).
 *
 * @param frad Frame Address.
 * @param n Number of frames to write.
 * @param buff The buffer which contains the data.
 * @param ff Write to flip-flop.
 */
void write_frames(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[], int ff);

/** Reverse len number of bits (from LSB) in num.
 *
 * @param num Number to reverse bits.
 * @param len Number of bits from LSB to reverse.
 * @return The reversed number.
 */
uint32_t reverseBits(uint32_t num, int len);

#endif /* SRC_JTAG_ENGINE_H_ */

/** @}*/
